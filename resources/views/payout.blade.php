@include('header')

<h1>SUCCESS</h1>
<p>Pembayaran sebesar : {{$data['value']}}</p>
<br><br>
@if($type == 'order')
    @extends('orderSuccess')

@elseif($type =='payment')
    @extends('prepaidPayout')

@else
    404 not found
@endif