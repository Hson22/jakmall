@include('header')

<h1>SUCCESS</h1>
<p>Pembayaran sebesar : {{$data['value']}}</p>
@if($type == 'product')
    @include('orderSuccess')
@elseif($type =='payment')
    @include('paymentSuccess')

@else
    404 not found
@endif