
<html>
<head>
    <title>App Name - @yield('header')</title>
</head>
<body>

<div class="container">
    @yield('content')
</div>
</body>
</html>