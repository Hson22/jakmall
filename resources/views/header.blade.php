@yield('header')
<div class="header">
    <h1>HELLO {{\Illuminate\Support\Facades\Auth::user()->name}}</h1>
    <p>{{0}} total order</p>
    <a href="{{url('prepaid')}}">prepaid</a>
    <a href="{{url('product')}}">product</a>
    <br><br>
</div>
@yield('content')