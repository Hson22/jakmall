@include('header')
<form action="{{url('history')}}" method="post">
    @csrf <!-- {{ csrf_field() }} -->
    <div class="container">
        <label for="order_no"><b>order no</b></label>
        <input type="text" placeholder="Enter email" name="order_no" id="order_no" >
        <button type="submit">Login</button>
    </div>
</form>
<table>
    @foreach($data['transaction'] as $item)
        <tr>
            @if($item->type == 'payment')
                <td>{{$item->detail->value}} for {{$item->detail->mobile_phone}}</td>
            @else
                <td>{{$item->detail->product}} that costs {{$item->detail->price}}</td>
            @endif
            <td>{{$item->type}}</td>
            <td>{{$item->order_no}}</td>
            <td>{{$item->value}}</td>
            @if($item->status =='unpaid')
                <td> <button onclick="location.href = '{{url('prepaidPayout/'.$item->order_no)}}';" type="button" class="cancelbtn">bayar sekarang</button></td>
            @else
                <td>{{$item->status}}</td>
            @endif
        </tr>
    @endforeach
</table>

@for($i=1;$i<=$data['page'];$i++)

    <a href="{{url('history/'.$i)}}">{{$i}}</a>
@endfor