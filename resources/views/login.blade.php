<form action="{{url('login')}}" method="post">
    @csrf <!-- {{ csrf_field() }} -->
    <div class="container">
        <label for="email"><b>email</b></label>
        <input type="text" placeholder="Enter email" name="email" id="email" required>

        <label for="psw"><b>Password</b></label>
        <input type="password" placeholder="Enter Password" name="password" id="password" required>

        <button type="submit">Login</button>
    </div>
</form>
<div class="container" style="background-color:#f1f1f1">
    <button onclick="location.href = '{{url('register')}}';" type="button" class="cancelbtn">register</button>
</div>
