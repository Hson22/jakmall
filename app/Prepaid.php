<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prepaid extends Model
{
    protected $table = 'prepaid';
    protected $fillable = [
        'user_id',
        'email',
        'mobile_phone',
        'value',
        'status',
        'order_no',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
    ];
}
