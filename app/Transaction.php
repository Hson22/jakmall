<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transaction';
    protected $fillable =[
        'user_id',
        'email',
        'order_no',
        'status',
        'value',
        'type',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];
    public function detail()
    {
        switch($this->attributes['type']) {
            case 'prepaid':
                $relation = $this->hasOne('App\Prepaid','order_no','order_no');
                break;
            case 'product':
                $relation = $this->hasOne('App\Product','order_no','order_no');
                break;
        }
        return $relation;
    }
}
