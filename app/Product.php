<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    protected $fillable = [
        'user_id',
        'email',
        'product',
        'shipping_address',
        'price',
        'order_no',
        'shipping_no',
        'status',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
    ];
}
