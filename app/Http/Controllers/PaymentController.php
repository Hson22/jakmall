<?php

namespace App\Http\Controllers;

use App\GenerateOrder;
use App\Prepaid;
use App\Product;
use App\Repository;
use App\Transaction;
use Illuminate\Http\Request;
use DB;
class PaymentController extends Controller
{

    function __construct(Prepaid $prepaid,Product $product,Transaction $transaction)
    {
        $this->prepaid = new Repository($prepaid);
        $this->product = new Repository($product);
        $this->transaction = new Repository($transaction);
    }

    public function payout(Request $request)
    {
        DB::beginTransaction();
        $data['status']='paid';
        $transaction = $this->transaction->findSpecific(['order_no'=>$request->order_no]);
        if($transaction) {
            if($transaction->type == 'prepaid') {
                $this->prepaid->update($data,$transaction->order_no,'order_no');
            } else {
                $data['shipping_no'] = GenerateOrder::generate('product',10);
                $this->product->update($data,$transaction->order_no,'order_no');
            }

            $this->transaction->update($data,$transaction->order_no,'order_no');
            DB::commit();
        } else {
            return 'transaction not found';
        }
    }

}
