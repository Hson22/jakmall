<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            // Store specific data to session
            session([
                'email' => Auth::user()->email,
                'name' => Auth::user()->name
            ]);

            return view('prepaid');
        }
            return view('login')->with('message','Username atau Password salah');
    }

    public function logout() {
        Auth::logout();
        return Redirect::route('login');
    }
}
