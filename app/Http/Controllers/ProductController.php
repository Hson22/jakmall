<?php

namespace App\Http\Controllers;

use App\GenerateOrder;
use App\Product;
use App\Repository;
use App\Transaction;
use Illuminate\Http\Request;

use DB;
use Exception;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{

    function __construct(Product $product,Transaction $transaction)
    {
        $this->product = new Repository($product);
        $this->transaction = new Repository($transaction);

    }

    public function create(Request $request)
    {
        try{
            DB::beginTransaction();
            $data = $request->all();
            $data['order_no'] = GenerateOrder::generate('product',10);
            $data['email'] = Auth::user()->email;
            $data['user_id'] = Auth::user()->id;
            $data['created_by'] = $data['email'];
            $data['status'] = 'unpaid';
            $data['price'] = $data['price']+10000;
            $data['value'] = $data['price'];
            $data['type']='product';
            DB::commit();
            $this->transaction->create($data);
            $this->product->create($data);
            return view('success',['type'=>'product','data'=>$data]);
        } catch (Exception $e) {
            DB::rollback();
        }
    }

    public function payOrder(Request $request)
    {
        $category['email'] = Auth::user()->email;
        $category['order_no'] = $request->order_no;
        $category['status'] = 'unpaid';
    }
}
