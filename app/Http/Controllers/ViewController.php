<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ViewController extends Controller
{
    public function loginVIew()
    {
        return view('login');
    }
    public function registerView()
    {
        return view('register');
    }
    public function prepaidView()
    {
        return view('prepaid');
    }

    public function productView()
    {
        return view('product');
    }

    public function successView($type)
    {
        return view('success',['type'=>$type]);
    }
    public function prepaidPayout($id)
    {
        return view('prepaidPayout',['order_no'=>$id]);
    }
}
