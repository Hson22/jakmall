<?php

namespace App\Http\Controllers;

use App\Repository;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{

    function __construct(Transaction $transaction)
    {
        $this->transaction = new Repository($transaction);
    }



    public function history($page)
    {
        $email = Auth::user()->email;
        $start = ($page-1)*20;
        $total = $this->transaction->total($email);

        $data['page'] = (int)$this->pagination($total);
        $data['transaction'] = $this->transaction->paginationWithRelation(20,$start,['email'=>$email],'detail');
        return view('history',['data'=>$data]);
    }
    private function pagination($total)
    {
        if($total<20){
            $page = 1;
        } else {
            $page =(($total%20)==0)?$total/20:$total/20+1;
        }
        return $page;
    }

    public function search(Request $request)
    {
        $id = $request->order_no;
        $email = Auth::user()->email;
        $total = $this->transaction->total($email);
        $data['page'] = (int)$this->pagination($total);
        $data['transaction'] = Transaction::where('email',$email)->where('order_no','like','%'.$id.'%')
        ->orderBy('created_at','desc')->get();
        return view('history',['data'=>$data]);
    }
}
