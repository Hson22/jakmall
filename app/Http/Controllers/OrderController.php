<?php

namespace App\Http\Controllers;

use App\Prepaid;
use App\Product;
use App\Repository;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    function __construct(Prepaid $prepaid,Product $product)
    {
        $this->balance = new Repository($prepaid);
        $this->product = new Repository($product);
    }


    public function showOrder($page=null,$limit=null,$orderNo='')
    {
        if(empty($orderNo)) {
            $balance = $this->balance->pagination($limit,($page-1)*$limit);
            $product = $this->product->pagination($limit,($page-1)*$limit);
            $response = $balance->merge($product)->sortByDesc('created_at');
        } else {
            $data['order_no'] = $orderNo;
            $response = $this->product->findBulkSpecific($data);
        }
        return $response;
    }
}
