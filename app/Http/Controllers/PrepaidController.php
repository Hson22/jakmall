<?php

namespace App\Http\Controllers;

use App\GenerateOrder;
use App\Prepaid;
use App\Transaction;
use Illuminate\Http\Request;
use App\Repository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PrepaidController extends Controller
{

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'value' => ['required',Rule::in([50000,100000,150000])],
            'mobile_phone' => ['required', 'string','min:7', 'max:12'],
        ]);
    }
    function __construct(Prepaid $prepaid,Transaction $transaction)
    {
        $this->prepaid = new Repository($prepaid);
        $this->transaction = new Repository($transaction);
    }

    public function create(Request $request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        $data['email'] = Auth::user()->email;
        $data['order_no'] = GenerateOrder::generate('prepaid',10);
        $data['status'] = 'unpaid';
        $data['type']='prepaid';
        $data['created_by'] = $data['email'];
        $validate = $this->validator($data);
        if($validate->fails() == false){
            $this->transaction->create($data);
            $this->prepaid->create($data);
            $data['value'] = $data['value'] *1.05;
            return view('success',['type'=>'payment','data'=>$data]);
        } else {
            print $validate->errors();
            header("Refresh:2; url=".url('prepaid'));
        }
    }
}
