<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 3/2/2019
 * Time: 4:27 PM
 */

namespace App;


class GenerateOrder {


    public static function generate($type,$length)
    {
        switch($type) {
            case 'prepaid':
                $string=1;
                break;
            case 'product':
                $string =2;
                break;
        }
        $characters = [0,1,2,3,4,5,6,7,8,9];
        $max = count($characters) - 1;
        for ($i = 0; $i < $length-1; $i++) {
            $string .= $characters[mt_rand(0, $max)];
        }
        return $string;
    }
}