<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 2/28/2019
 * Time: 9:22 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Repository
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function create(array $data)
    {
        $object = $this->model->fill($data);
        $object->save();
        return $object;
    }

    public function update(array $data, $id,$pk)
    {
        $record = $this->model->where($pk,$id)->first();
        return $record->update($data);
    }
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function show($id,$column)
    {
        return $this->model->where($column,$id);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    public function findSpecific(array $data)
    {
        $query = $this->model;
        foreach($data as $key=>$item)
        {
            $query = $query->where($key,$item);
        }
        return $query->first();
    }


    public function findBulkSpecific(array $data)
    {
        $query = $this->model;
        foreach($data as $key=>$item)
        {
            $query = $query->where($key,$item);
        }
        return $query->get();
    }

    public function total($email)
    {
        return $this->model->where('email',$email)->count();
    }
    public function paginationWithRelation($limit,$start,$condition,$relation)
    {
        $query = $this->model->skip($start)->take($limit)->orderBy('created_at','desc');
        foreach($condition as $key=>$item)
        {
            $query = $query->where($key,$item);
        }
        return $query->get();
    }
}