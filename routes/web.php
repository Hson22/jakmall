<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('/login','LoginController@login');
Route::get('/register','ViewController@registerView');
Route::post('/register','RegisterController@register');
Route::get('login','viewController@loginVIew')->name('login');
Route::group(['middleware' => 'auth'], function () {
    Route::get('/prepaid','ViewController@prepaidView');
    Route::post('/prepaid','PrepaidController@create');
    Route::get('/product','ViewController@productView');
    Route::post('/product','ProductController@create');
    Route::get('/successOrder/{type}','ViewController@successView');
    Route::get('/payment','ViewController@paymentView');
    Route::get('history/{page}','TransactionController@history');
    Route::post('history','TransactionController@search');
    Route::get('prepaidPayout/{order_no}','ViewController@prepaidPayout');
    Route::post('payout','PaymentController@payout');
});
